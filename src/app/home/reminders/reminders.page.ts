import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Reminder } from './reminder.model';
import { Subscription } from 'rxjs';
import { RemindersService } from './reminders.service';
import { CalendarComponent } from 'ionic2-calendar/calendar';

@Component({
  selector: 'app-reminders',
  templateUrl: './reminders.page.html',
  styleUrls: ['./reminders.page.scss'],
})
export class RemindersPage implements OnInit, OnDestroy {
  @ViewChild(CalendarComponent, {static: true}) myCal: CalendarComponent;
  remindersSub: Subscription;
  isLoading = false;
  loadedReminders: Reminder[];
  minDate = new Date().toISOString();

  calendar = {
    mode: 'month',
    currentDate: new Date(),
  };

  constructor(private remindersService: RemindersService) { }

  // Get alle reminders when initializing the screen
  ngOnInit() {
    this.remindersSub = this.remindersService.reminders.subscribe(reminders => {
      this.loadedReminders = reminders;
    });
  }

  // Is called after ngOnInit
  ionViewDidEnter() {
    this.isLoading = true;
    this.remindersService.fetchReminders().subscribe(() => {
      this.isLoading = false;
    });
  }

  // Is called when closing the screen
  ngOnDestroy() {
    this.remindersSub.unsubscribe();
  }

  changeMode(mode: string) {
    this.calendar.mode = mode;
  }

  back() {
    const mySwiper = document.querySelector('.swiper-container')['swiper'];
    mySwiper.slidePrev();
  }

  next() {
    const mySwiper = document.querySelector('.swiper-container')['swiper'];
    mySwiper.slideNext();
  }
}
