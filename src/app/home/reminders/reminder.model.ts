export class Reminder {
    constructor(
        public id: string,
        public userId: string,
        public title: string,
        public description: string,
        public startTime: Date,
        public endTime: Date,
        public allDay: boolean,
    ) {}
}
