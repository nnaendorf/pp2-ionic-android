import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RemindersPage } from './reminders.page';

const routes: Routes = [
  {
    path: '',
    component: RemindersPage
  },
  {
    path: 'create-reminders',
    loadChildren: () => import('./create-reminders/create-reminders.module').then( m => m.CreateRemindersPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RemindersPageRoutingModule {}
