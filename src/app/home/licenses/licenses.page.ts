import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { LicensesService } from './licenses.service';
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';

function base64toBlob(base64Data, contentType) {
  contentType = contentType || '';
  const sliceSize = 1024;
  const byteCharacters = window.atob(base64Data);
  const bytesLength = byteCharacters.length;
  const slicesCount = Math.ceil(bytesLength / sliceSize);
  const byteArrays = new Array(slicesCount);

  for (let sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
    const begin = sliceIndex * sliceSize;
    const end = Math.min(begin + sliceSize, bytesLength);

    const bytes = new Array(end - begin);
    for (let offset = begin, i = 0; offset < end; ++i, ++offset) {
      bytes[i] = byteCharacters[offset].charCodeAt(0);
    }
    byteArrays[sliceIndex] = new Uint8Array(bytes);
  }
  return new Blob(byteArrays, {type: contentType});
}

@Component({
  selector: 'app-licenses',
  templateUrl: './licenses.page.html',
  styleUrls: ['./licenses.page.scss'],
})
export class LicensesPage implements OnInit {
  form: FormGroup;
  isFrontside = true;

  constructor(private licensesService: LicensesService, private loadingCtrl: LoadingController, private router: Router) { }

  // Is called when entering the screen
  ngOnInit() {
    this.form = new FormGroup({
      frontside: new FormControl(null),
      backside: new FormControl(null)
    });
  }

  // Is called when image is picked
  onImagePicked(imageData: string) {
    let imageFile;
    try {
      // Transforms the image data to a blob
      imageFile = base64toBlob(imageData, 'image/jpeg');
    } catch (error) {
      console.log(error);
      return;
    }
    if (this.isFrontside) {
      this.form.patchValue({frontside: imageFile});
    } else {
      this.form.patchValue({backside: imageFile});
    }
  }

  // Is called when submitting the license
  onSubmitLicense() {
    this.loadingCtrl.create({
      message: 'Führerschein wird eingereicht...'
    })
    .then(loadingEl => {
      // Loading spinner
      loadingEl.present();
      // Calls the upload function
      this.licensesService.uploadImages(
        this.form.get('frontside').value,
        this.form.get('backside').value
      ).subscribe(() => {
        // Is called after the image has been uploaded
        loadingEl.dismiss();
        this.form.reset();
        this.router.navigate(['/home']);
        }, error => {
          console.log("Session nicht mehr aktiv");
        });
    });
  }

  switchToFrontside() {
    this.isFrontside = true;
  }

  switchToBackside() {
    this.isFrontside = false;
  }
}
