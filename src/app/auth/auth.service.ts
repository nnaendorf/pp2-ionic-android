import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { User } from './user.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Plugins } from '@capacitor/core';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ReminderData } from '../home/reminders/reminders.service';

export class AuthResponseData {
  email: string;
  session: string;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
private _user = new BehaviorSubject<User>(null);
private activeLogoutTimer: any;

  constructor(private http: HttpClient, private router: Router) { }

  // Offers the userId as an observable, so it can be used 
  // anywhere in the app, thanks to rxjs
  get userId() {
    return this._user.asObservable().pipe(
      map(user => {
        if (user) {
          return user.email;
        } else {
          return null;
        }
      })
    );
  }

  // Offers the information if a user is authenticated as
  // an observable
  get userIsAuthenticated() {
    return this._user.asObservable().pipe(
      map(user => {
        if(user) {
          return true;
          //return true;
        } else {
          return false;
          //return true;
        }
      })
    )
  }

  // Function to compare the active user with another one
  userEqualsTo(resData: {[key: string]: ReminderData}, key: string) {
    return this._user.asObservable().pipe(
      map(user => {
        if (user.email === resData[key].userId) {
          return true;
        } else {
          return false;
        }
      })
    );
  }

  // Called when the login button has been pressed
  login(email: string, password: string) {

    const stringToEncode = email + ':' + password;
    const base64encodedAuth = btoa(stringToEncode);
    console.log(base64encodedAuth);

    const httpOptions = {
      // Credentials are being transmitted, base64 encoded, in the http header
      headers: new HttpHeaders({
        'Authorization': 'Basic ' + base64encodedAuth
      }),
      observe: 'response' as 'body',
      withCredentials: true
    };
    // Get-Request to the API
    return this.http.get(
      'http://212.227.10.211:8080/pp-app-server/authentication', httpOptions
    );

  }

  // Deletes the token and the active user information
  logout() {
    if (this.activeLogoutTimer) {
      clearTimeout(this.activeLogoutTimer);
    }
    this._user.next(null);
    Plugins.Storage.remove({key: 'authData'});
    this.router.navigateByUrl('/auth');
  }

  // Sets the active user data
  setUserData(userData: AuthResponseData) {
    const user = new User(
      userData.email
    );
    this._user.next(user);
  }
}
